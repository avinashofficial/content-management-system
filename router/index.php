<?php

	header( 'Content-Type: text/html; charset=utf-8' );

	if(file_exists('lib/configuration/index.php')) {

        /** Including configuration file file  */

        require_once 'lib/configuration/index.php';

    } else {

        echo '<h1>Something is wrong while including configuration files</h1>';

	}
	
	/** Redirect all double slash url request to single slash */

	if(strpos(BASE_URL, '//')!==FALSE) {

		$redirectURL=str_replace('//', '/', BASE_URL);
		header("Location: $redirectURL", TRUE, 301);
		exit();

	}
	
	/** Manage All POST method request */

	if($_SERVER['REQUEST_METHOD']=="POST") {

		require_once 'formManager.php';
		exit();

	}
	
	/** Check website domain status */

	require_once $_SERVER['DOCUMENT_ROOT'].'/lib/modules/siteStatus.php';
	
	switch ($urlStructure[0]) {

		case ''	:
			require_once 'homepage.php';
			break;

		case 'election-result-2019' :
			require_once 'election.php';
			break;

		case 'api'	:
			require_once 'api.php';
			break;

		case 'search' :
			require_once 'search.php';
			break;
		
		case 'feed' :
			require_once 'feed.php';
			break;
		
		case 'tag' :
			require_once 'tag.php';
			break;
		
		case 'amp' :
			require_once 'amp.php';
			break;
		
		case 'mobile' :
			require_once 'mobile.php';
			break;
		
		case 'category' :
			require_once 'category.php';
			break;

		case (preg_match('/sitemap.xml/', $urlStructure[0]) ? true : false) :
			require_once 'sitemap.php';
			break;

		case 'dashboard' :
			require_once 'admin.php';
			break;

		case 'email' :
			require_once 'email.php';
			break;

		/** For all type of verification process */

		case 'verify' :
			require_once 'verify.php';
			break;

		case 'logout' :
			require_once 'logout.php';
			break;
		
		default :
			require_once 'routerManager.php';
			break;

	}

	exit();