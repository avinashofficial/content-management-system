<?php

    $tag_name=str_replace('-', ' ', mysqli_real_escape_string($connection, $_GET['q']));
    
    if($tagList=mysqli_query($connection, "SELECT DISTINCT(link_relation_id), link_status, link_url, post_image, post_title, meta_description FROM {$table_prefix}tag, {$table_prefix}link, {$table_prefix}post, {$table_prefix}meta WHERE tag_name='{$tag_name}' AND link_relation_id=post_id AND link_relation_id=tag_relation_id AND tag_type='post' AND link_type='post' AND tag_relation_id=post_id AND post_id=meta_relation_id AND link_status=200 ORDER BY link_published DESC LIMIT 0, 30")) {

        if(mysqli_num_rows($tagList)!=0) {

            while($tagPostData=mysqli_fetch_assoc($tagList)) {

                $tagPost[]=$tagPostData;

            }

            $tag_path=USER_VIEW_PATH.$urlStructure[1].'-category.php';

            /** Include Tag view template */

            if(file_exists($tag_path)) {

                require_once $tag_path;

            } else {

                require_once USER_VIEW_PATH.'tag.php';

            }

        } else {

            header("Location: /", TRUE, 301);

        }

    }

	exit();