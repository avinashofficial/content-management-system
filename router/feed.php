<?php

	header('Content-Type: application/xml; charset=utf-8');

	$feedURL=str_replace('feed/', '', BASE_URL);

	$feedPath=RESOURCE.'feeds/';

	if($feedURL!='/') {

		switch($urlStructure[1]) {

			case 'tag' : require_once $feedPath.'tag.php';
						 break;

			case 'instant-article' : require_once $feedPath.'default.php';
									 break;

			default : require_once $feedPath.'single-default.php';
					  break;

		}

	} else {

		/** Recent post from website */

		require_once RESOURCE.'feeds/default.php';

	}
	
	exit();