<?php
	
	/* Project root directory of the server */

	define('ROOT', dirname(__FILE__));

	/* database connection setting */

	define('DB_HOST', 'localhost');		// database hostname ex :- hostname
	define('DB_USER', 'root');		// database username ex :- root
	define('DB_PASS', '');		// datapase user password :- 12345
	define('DB_NAME', 'cms');		// database name ex :- cms_db

	/* database table prefix */

	$table_prefix = 'isw_';

	/* dafault timezone is asia calcutta for indian user if you want to change timezone please read http://php.net/manual/en/timezones.php */

	$time_zone = 'Asia/Calcutta';
	$time_zone_value = '+5:30';

	/* Error Reporting */

	define('DEVELOPMENT_MODE', true); // Error reporting true to show error or false to hide error

	/** user IP Address */

	define('IP', $_SERVER['REMOTE_ADDR']);
	
?>