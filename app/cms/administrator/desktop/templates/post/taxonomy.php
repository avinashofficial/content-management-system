<?php
	
    $category_id=mysqli_real_escape_string($connection, $_GET['id']);
    $taxonomyType=mysqli_real_escape_string($connection, $_GET['taxonomy']);
    
	if($postDetails=mysqli_query($connection, "SELECT * FROM {$table_prefix}category, {$table_prefix}link, {$table_prefix}meta WHERE link_type='category' AND link_relation_id=category_id AND meta_relation_id=category_id AND category_id='{$category_id}' AND link_status!=301 AND link_type='category'  AND meta_type='{$taxonomyType}' LIMIT 1")) {
		if(mysqli_num_rows($postDetails)!=0) {
			extract(mysqli_fetch_assoc($postDetails));
		} else { ?>
			<script type="text/javascript">
				alert('Invalid action found');
				window.location.href='dashboard/post/?type=category';
			</script>
		<?php
		}
	}

?>
<h2>Update <?php echo (($link_status=='206') ? 'Drafted ' : '').' Category'; ?> <span><a href="<?php echo $postType ?>/?type=category" class='btn btn-blue btn-sm'>Add New</a></span></h2>

<div class="form-box">
	<form method="POST" id="formData" enctype="multipart/form-data" >
		<div class="left-box">
			<div class="form-group">
				<input type="text" class="form-control inside-shadow" name="title" placeholder="Title" value="<?php echo $category_title ?>" required="" >
			</div>
			<div class="form-group">
				<div class="url">
					<?php
						$url=explode('/', ltrim(rtrim($link_url, '/'), '/'));
					?>
					<p><strong>Permalink</strong></p><p><span><?php echo SITE_DOMAIN.'/...'; ?>/</span></p>
					<input type="text" id="url" class="form-control" value="<?php echo end($url); ?>" name="url" >
					<input type="hidden" name='oldURL' value='<?php echo $link_url ?>' >
				</div>
			</div>
			<div class="form-group fullscreen-editor">
				<textarea name="description" class="editor inside-shadow" ><?php echo html_entity_decode($category_description); ?></textarea>
			</div>
			<section>
				<h3>Advance Options for digital marketing</h3>
				<ul class='tabs'>
					<li id="seo" class='active' >Search Engine Optimization</li>
					<li id="fb" >Facebook</li>
					<li id="tw" >Twitter</li>
				</ul>
				<div class="seo">
					<div class="form-group">
						<label>Meta Title</label>
						<input type="text" name="meta_title" value="<?php echo $meta_title; ?>" id="meta_title" class="form-control inside-shadow"  placeholder="Meta title" >
					</div>
					<div class="form-group">
						<label>Meta Description</label>
						<textarea name="meta_desc" id="meta_desc" class="form-control inside-shadow" placeholder="Meta Description" ><?php echo $meta_description ?></textarea>
					</div>
					<div class="form-group">
						<label>Meta Keywords</label>
						<textarea name="meta_key" id="meta_keywords" class="form-control inside-shadow" placeholder="Enter keywords of the post ( use , to seprate keywords )" ><?php echo $meta_keyword ?></textarea>
					</div>
					<div class="form-group">
						<label>Allow search engines to show this Post in search results?</label>
						<select name="robots" class="form-control inside-shadow">
							<option value="index, follow" <?php echo (($meta_robots=='index, follow') ? 'selected' : '' ) ?> >Yes</option>
							<option value="noindex, nofollow" <?php echo (($meta_robots=='noindex, nofollow') ? 'selected' : '' ) ?> >No</option>
						</select>
					</div>
				</div>
				<div class="fb" style="display: none;">
					<div class="facebook-image" style="display: <?php echo (($fb_img=='') ? 'none' : 'block' ) ?>;" >
						<input type="hidden" name="oldFacebookImage" value="<?php echo $fb_img ?>">
						<div class="img"><img src="<?php echo $fb_img; ?>" id="facebook-image" alt=""></div>
						<p id="facebookChangeImage" class='changeImage' >Remove facebook feature image</p>
					</div>
					<div class="facebookUploadBox" style="display: <?php echo (($fb_img=='') ? 'block' : 'none' ) ?>;">
						<span>Set facebook feature image</span>
						<input type='file' id="fbImage" name='facebookImage' />
					</div>
					<div class="form-group">
						<label>Facebook Title</label>
						<input type="text" name="fb_title" class="form-control inside-shadow" value="<?php echo $fb_title ?>" placeholder="Facebook title" >
					</div>
					<div class="form-group">
						<label>Facebook Description</label>
						<textarea name="fb_desc" id="fb_desc" class="form-control inside-shadow" placeholder="Facebook Description" ><?php echo $fb_desc ?></textarea>
					</div>
				</div>
				<div class="tw" style="display: none;">
					<div class="twitter-image" style="display: <?php echo (($fb_img=='') ? 'none' : 'block' ) ?>;">
						<input type="hidden" name="oldTwitterImage" value="<?php echo $tw_img ?>">
						<div class="img"><img src="<?php echo $tw_img ?>" id="twitter-image" alt=""></div>
						<p id="twitterChangeImage" class="changeImage" >Remove twitter feature image</p>
					</div>
					<div class="twitterUploadBox" style="display: <?php echo (($fb_img=='') ? 'block' : 'none' ) ?>;">
						<span>Set twitters feature image</span>
						<input type='file' id="twImage" name='twitterImage' />
					</div>
					<div class="form-group">
						<label>Twitter Title</label>
						<input type="text" name="tw_title" class="form-control inside-shadow" value="<?php echo $tw_title; ?>" placeholder="Twitter title" >
					</div>
					<div class="form-group">
						<label>Twitter Description</label>
						<textarea name="tw_desc" id="tw_desc" class="form-control inside-shadow" placeholder="Twitter Description" ><?php echo $tw_desc ?></textarea>
						</div>
				</div>
			</section>
		</div>
		<div class="right-box">
			<div class="action-box inside-shadow">
				<input type="button" id="updateTaxonomy" class="btn btn-green btn-block" value="Update Category">
				<input type="hidden" name="website_id" value="<?php echo SITE_ID ?>" >
				<input type="hidden" name="website_user_id" value="<?php echo USER_ID ?>" >
				<input type="hidden" name="type" value="draft" >
				<input type="hidden" name="postType" value="<?php echo $taxonomyType ?>" >
				<input type="hidden" name="category_id" value="<?php echo $category_id ?>" >
				<input type="hidden" name="action" value="taxonomy" >
				<a href='<?php echo SITE_URL.$link_url; ?>' class='btn btn-block btn-blue' target='_blank' >View Live Category</a>
			</div>
			<h2>Tags</h2>
			<?php
				$tagPostID=(($category_parent_id==0) ? $category_id : $category_parent_id );
				
				if($tagList=mysqli_query($connection, "SELECT tag_name FROM {$table_prefix}tag WHERE tag_relation_id='{$tagPostID}' AND tag_site_id='".SITE_ID."' AND tag_type='category'")) {
					if(mysqli_num_rows($tagList)!=0) {
						while($tagsName=mysqli_fetch_assoc($tagList)) {
							$tags[]=$tagsName['tag_name'];
						}
						$tags=implode(',', $tags);
					} else {
						$tags='';
					}
				}
			?>
			<div class="action-box inside-shadow">
				<div class="form-group">
					<input type="text" class='tags' />
					<input type="hidden" id="tagValue" value="<?php echo $tags; ?>" >
					<input type="hidden" id='tags' name="tags" value='' >
				</div>
			</div>
			<div class="tagList" style="display: none;"></div>
			<h2>Feature Image</h2>
			<div class="form-group upload-btn" style="display: <?php echo (($category_image!='') ? 'none' : 'block') ?>;">
				<span>Set Feature Image</span>
				<input type="hidden" name="oldFeatureImage" value="<?php echo $category_image ?>">
				<input type="file" id="featureImage" name="featureImage">
				<p><strong>Only these file formats are supported:</strong> .png, .jpg, .jpeg, .gif, .doc, .docx, .xls, .xlsx, .pdf, .txt, .csv <strong>Maximum upload limit is 300 MB</strong></p>
			</div>
			<div class="img-preview action-box" style="display: <?php echo (($category_image!='') ? 'block' : 'none') ?>;">
				<img src="//<?php echo SITE_DOMAIN.$category_image ?>" id="previewImage" alt="">
				<p id="changeImage" class="changeImage" >Remove feature image</p>
			</div>
		</div>
	</form>
</div>