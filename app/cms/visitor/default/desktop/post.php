<?php

    include_once 'header.php';

?>

<!-- Default post view -->

<section class='body'>

    <div class="main">
        <div class='title'>
            <h2><?php echo $category_title; ?></h2>
        </div>
        <h1><?php echo $post_title; ?></h1>

        <!-- Author Box -->
        <div class='post-info'>
            <div class="author-img"><img src="<?php echo (($profile_image=='') ? USER_IMAGES.'default-profile.png' : $profile_image ); ?>" alt=""></div>
            <div class="author-name"><p><?php echo $profile_display_name; ?></p></div>
            <div class="post-update"><p><strong>Updated </strong> <?php echo date('D, dS M Y h:i A T', strtotime('+5 hour +30 minutes', strtotime($link_modified))) ?></p></div>
            <div class="post-share">
                <div class="social-link">
                    <a href="https://www.facebook.com/sharer.php?u=<?php echo SITE_URL.BASE_URL ?>" target='_blank' ><i class="fab fa-facebook-square"></i></a>
                    <a href="https://twitter.com/intent/tweet?url=<?php echo SITE_URL.BASE_URL ?>&text=<?php echo $post_title ?>&hashtags=<?php echo str_replace(' ', '', $tagName) ?>" target='_blank' ><i class="fab fa-twitter-square"></i></a>
                    <a href="https://reddit.com/submit?url=<?php echo SITE_URL.BASE_URL ?>&title=<?php echo $post_title ?>" target='_blank' ><i class="fab fa-reddit-square"></i></a>
                    <a href="http://pinterest.com/pin/create/link/?url=<?php echo SITE_URL.BASE_URL ?>&media=<?php echo urlencode(HTTP_PROTOCOL.SITE_DOMAIN.$post_image); ?>&description=<?php echo ((isset($meta_description)) ? (($meta_description!='') ? $meta_description : SITE_DESC ) : SITE_DESC ) ?>" target='_blank' ><i class="fab fa-pinterest-square"></i></a>
                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo SITE_URL.BASE_URL ?>" target='_blank' ><i class="fab fa-linkedin"></i></a>
                </div>
            </div>
        </div>
        
        <div class="feature-img">
            <img src="//<?php echo SITE_DOMAIN.$post_image ?>" alt="">
        </div>
        <div class="content">
            <p><?php echo html_entity_decode($post_description); ?></p>
        </div>

        <div class="comment" style="margin-top: 20px" >
            <a href=""><img src="<?php echo USER_IMAGES ?>app-download-banner.jpg" alt=""></a>
        </div>

        <div class="tags">
            <?php
                $tags=explode(', ', $tagName);
                for($i=0; $i<sizeof($tags); $i++) {
                    if($tags[$i]!='') {
                        echo "<a href='tag/".str_replace(' ', '-', strtolower($tags[$i]))."/' class='english' >{$tags[$i]}</a>";
                    }
                }
            ?>
        </div>

        <div class="comment">
                                   
        </div>
        
        <!-- You plus -->

        <div id="yp_widget"></div>

        <div class="taboola">
            <div id="taboola-below-article-thumbnails"></div>
            <script type="text/javascript">
            window._taboola = window._taboola || [];
            _taboola.push({
                mode: 'thumbnails-a',
                container: 'taboola-below-article-thumbnails',
                placement: 'Below Article Thumbnails',
                target_type: 'mix'
            });
            </script>
        </div>

    </div>

    <div class="sidebar">
        <?php
        
            require_once 'sidebar.php';

        ?>
    </div>

</section>

<?php

    include_once 'footer.php';