<?php

	$categoryID=mysqli_real_escape_string($connection, ((isset($_GET['category_id'])) ? $_GET['category_id'] : 0 ));
	if($categoryData=mysqli_query($connection, "SELECT category_parent_id, category_name, category_description, link_url FROM {$table_prefix}category, {$table_prefix}link WHERE category_id=link_relation_id AND category_id='{$categoryID}' AND link_type='category' AND link_status=200")) {
		if(mysqli_num_rows($categoryData)!=0) {
			extract(mysqli_fetch_assoc($categoryData));
		} else {
			$category_name='';
			$category_description='';
			$category_parent_id='';
			$link_url='';
		}
	}

?>
<h2><?php echo ucwords(rtrim(ltrim(ADMIN_BASE_URL, '/'), '/')); ?> Categories <?php echo ((isset($_GET['category_id'])) ? "<span><a href='post/?type=category' class='btn btn-blue btn-sm' >Add New</a></span>" : '' ) ?></h2>

<div class="row">
	<div class="category">
		<div class="form-box">
			<form method="POST" id="categoryData" >
				<h3><?php echo ((isset($_GET['category_id'])) ? 'Update' : 'Add New' ) ?> Category</h3>
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" <?php echo ((!isset($_GET['category_id'])) ? 'id="title"' : ''); ?> class="form-control inside-shadow" placeholder="Category name..." value="<?php echo $category_name; ?>" autofocus >
				</div>
				<div class="form-group">
					<label>Permalink</label>
					<input type="text" name="url" id='url' class="form-control inside-shadow" placeholder="Category url..." value="<?php $categoryURL=explode('/', ltrim(rtrim($link_url, '/'), '/')); echo end($categoryURL); ?>" >
				</div>
				<div class="form-group">
					<label>Parent Category</label>
					<select name="category" class="form-control inside-shadow">
						<option value="0">Select Parent Category</option>
						<?php
							if($categoryList=mysqli_query($connection, "SELECT category_id, category_name, link_url FROM {$table_prefix}category, {$table_prefix}link WHERE category_site_id='".SITE_ID."' AND link_type='category' AND link_relation_id=category_id AND category_id!='{$categoryID}' AND link_status=200")) {
								while($category=mysqli_fetch_assoc($categoryList)) { ?>
										<option value="<?php echo $category['category_id'].'_'.str_replace(SITE_DOMAIN.'/category', '', $category['link_url']); ?>" <?php echo (($category['category_id']==$category_parent_id) ? 'selected' : '' ) ?> > <?php echo ucwords($category['category_name']); ?></option>
								<?php
								}
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea name="description" class="form-control inside-shadow" placeholder="About category..."><?php echo $category_description; ?></textarea>
				</div>
				<div class="form-group">
					<input type="hidden" name="type" value="category">
					<?php
						if(isset($_GET['category_id'])) {
							echo "<input type='hidden' name='oldURL' value='{$link_url}' >";
						}
					?>
					<input type="hidden" name="postType" value="<?php echo $postType ?>">
					<input type="hidden" name="categoryID" id="category_id" value="<?php echo $categoryID; ?>">
					<input type="submit" id="submitCategory" class="btn btn-green" value="<?php echo (isset($_GET['category_id']) ? 'Update' : 'Add' ); ?> Category" >
					<?php
					if(isset($_GET['category_id'])) {
						echo "<a href='' class='btn btn-blue'>Advance Edit</a>";
					}
					?>
				</div>
			</form>
		</div>
		<div class="category-list">
			<table id="table">
				<thead>
					<tr>
						<th style='width: 200px;'>Title</th>
						<th>Description</th>
						<th style='width: 50px'>Count</th>
					</tr>
				</thead>
				<tbody>
					<?php

						if($dataList=mysqli_query($connection, "SELECT category_id, category_description, category_name, category_total_count, link_url, profile_display_name FROM {$table_prefix}category, {$table_prefix}link, {$table_prefix}profile WHERE category_id=link_relation_id AND link_type='category' AND category_type='{$postType}' AND category_site_id='".SITE_ID."' AND profile_owner_id='".USER_ID."' AND link_status=200 ORDER BY category_id DESC")) {
							if(mysqli_num_rows($dataList)!=0) {
								while($data=mysqli_fetch_assoc($dataList)) { extract($data) ?>
									<tr>
										<td>
											<p><a href="post.php?type=edit&editor=true&post_id=<?php echo $category_id; ?>" ><?php echo $category_name ?></a></p>
											<div class="hover">
												<a href="post.php?type=category&category_id=<?php echo $category_id; ?>" ><i class="fas fa-pencil-alt"></i> Edit</a>
												<?php
													if($category_id!=1) { ?>
														<a href="delete.php?type=category&category_id=<?php echo $category_id; ?>"><i class="fas fa-trash"></i> Delete</a>
													<?php
													}
												?>
												<a href="<?php echo $link_url; ?>" target='_blank' ><i class="fas fa-eye"></i> View</a>
											</div>
										</td>
										<td><?php echo $category_description; ?></td>
										<td><?php echo $category_total_count; ?></td>
									</tr>
								<?php
								}
							}
						}

					?>
				</tbody>
			</table>
		</div>
	</div>
</div>