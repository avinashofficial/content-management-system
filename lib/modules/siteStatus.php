<?php

	/** Check software is already install or not */

	if($checkInstallation=mysqli_query($connection, "SELECT * FROM {$table_prefix}site WHERE site_domain='".SITE_DOMAIN."'")) {

		if(mysqli_num_rows($checkInstallation)==0) {

			/** Installtion step 1 */

			if($checkDomain=mysqli_query($connection, "SELECT * FROM {$table_prefix}site")) {

				if(mysqli_num_rows($checkDomain)==0) {

					/* Include installation file */
					$installFile1=$_SERVER['DOCUMENT_ROOT'].'/lib/install/step-1.php';
					if(file_exists($installFile1)) {
		
						require_once $installFile1;
						exit();

					} else {
						
						include_once ERROR_404;

					}

				} else {

					/** Check Domain */

					require_once 'checkDomain.php';

				}

			}

		} else {

			extract(mysqli_fetch_assoc($checkInstallation));

			if($site_status==0) {

				/** Step 2 of installation process */
				
				$installFile2=$_SERVER['DOCUMENT_ROOT'].'/lib/install/step-2.php';

				if(file_exists($installFile2)) {
	
					require_once $installFile2;
					exit();
	
				} else {
	
					die('Something went wrong while installing application on your server. kindly empty your database and restart installation process.');
	
				}

			}

		}

	} else {

		/** Import Database */

		require_once $_SERVER['DOCUMENT_ROOT'].'/lib/install/import-database.php';
		exit();
		
	}

?>