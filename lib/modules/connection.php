<?php
	
	if(file_exists($_SERVER['DOCUMENT_ROOT'].'/config.php')) {

		require_once $_SERVER['DOCUMENT_ROOT'].'/config.php';

		if(DEVELOPMENT_MODE==false) {

			/* hide errors message */

			error_reporting(0);
			ini_set('display_errors', 0);

		}

		/* Established database connection */

		if(!$connection=mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)) {
			echo "<h1>Error while establishing a database connection</h1>";
			exit();
		}

		/** Set default database collation */

		mysqli_set_charset($connection, 'utf8mb4');

	} else {

		echo '<h1>Configuration file is not found.</h1>';

	}

?>