<?php
	
	session_start();

	date_default_timezone_set($time_zone);
	
	define('CREDIT', 'Punjab Kesari');
	define('CREDIT_MAIL', 'avinash@itssoftword.com');
	define('CREDIT_URL', 'http://www.itssoftworld.com');

	/** detect user device type or browser */

	define('USER_DEVICE', $_SERVER['HTTP_USER_AGENT']);

	/** Current url of the website */

	define('ACTIVE_URL', (str_replace('', '', $_SERVER['REQUEST_URI']))); // echo complete url show on the url bar

	/** Use for url rounting in router index.php */

	$urlStructure=explode('/', rtrim(ltrim(ACTIVE_URL, '/'), '/'));

	/* Check for desktop or mobile website */

	// if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', USER_DEVICE)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr(USER_DEVICE,0,4))) {

	// 	define('SITE_VERSION', 'mobile');

	// }  else {

		define('SITE_VERSION', 'desktop');

	// }

	/** Define website default name and description according to domain */

	define('SITE_NAME', ((isset($site_name)) ? $site_name : CREDIT ));
	define('SITE_DESC', ((isset($site_description)) ? $site_description : 'Hindi News, Latest Hindi News by Punjab Kesari' ));
	
	/* SITE DOMAINS */
	
	define('HTTP_PROTOCOL', $_SERVER['REQUEST_SCHEME'].'://');

	define('SITE_DOMAIN', $_SERVER['HTTP_HOST']); // return only site domain with protocol
	define('SITE_URL', HTTP_PROTOCOL.$_SERVER['HTTP_HOST']); // return site domain without protocol

	if(sizeof(explode('.', SITE_DOMAIN))<3) {
		define('MAIN_DOMAIN', SITE_URL);
	} else {
		define('MAIN_DOMAIN', implode('.', array_slice(explode('.', SITE_URL), -2, 3, true))); // show only the actual domain like :- example.com
	}

	define('SITE_APP', ((isset($site_app)) ? $site_app : 'cms' )); // deafult all for visitors
	define('SITE_THEME', ((isset($site_theme)) ? $site_theme : 'default' ));	// default theme of website for visitor

	/** Use when user is logged in */

	$userApp=((isset($_COOKIE['user_app']) ? explode(CREDIT, base64_decode($_COOKIE['user_app'])) : '' ));
	define('USER_TYPE', ((isset($userApp[0])) ? $userApp[0] : 'administrator' ));	// directory of logged in user
	define('USER_APP', ((isset($userApp[1])) ? $userApp[1] : 'cms' )); // user type of logged in user

	/* URL Management */

	define('QUERY_URL', htmlspecialchars_decode($_SERVER['QUERY_STRING'])); // echo only query string of the url
	define('BASE_URL', str_replace(QUERY_URL, '', strtok(ACTIVE_URL, '?'))); // return only the actual url and remove query string

	/* Dashboard URL Management */

	define('ADMIN_ACTIVE_URL', str_replace('/dashboard', '', (str_replace('', '', $_SERVER['REQUEST_URI'])))); // echo complete url show on the url bar
	define('ADMIN_QUERY_URL', str_replace('/dashboard', '', $_SERVER['QUERY_STRING'])); // echo only query string of the url
	define('ADMIN_BASE_URL', str_replace('/dashboard', '', str_replace(QUERY_URL, '', strtok(ACTIVE_URL, '?')))); // return only the actual url and remove query string
	define('API_BASE_URL', str_replace('/api', '', str_replace(QUERY_URL, '', strtok(ACTIVE_URL, '?')))); // return only the actual url and remove query string

	/* Email Server setup configuration */

	define('SMTP_HOST', 'smtp-relay.sendinblue.com'); // host address of email server like :- mail.punjabkesari.com
	define('SMTP_SSL', 'false'); // is email server is using ssl if yes then change to true
	define('SMTP_PORT', '587'); // PORT id of their email server like :- 587, 465
	define('SMTP_USERNAME', 'avinash@itssoftworld.com'); // email user like :- contact@punjabkesari.com
	define('SMTP_PASSWORD', 'Ng7n1T0KZJkEF9z3'); // password of the email
	define('SMTP_EMAIL', 'donotreply@punjabkesari.com'); // Which email is display to receiver end like :- donotreply@punjabkesari.com
	define('SMTP_NAME', 'Punjab Kesari Delhi'); // Show Name on email :- ITS Soft World, Avinash Kumar
	define('REPLY_EMAIL', 'Punjab Kesari Delhi'); // Reply email where user can send a reply mail like :- contact@punjabkesari.com

	/** Sitemap Configuration */

	define('SITEMAP_LIMIT', 1000);
	define('PER_PAGE_LIMIT', 35);

?>