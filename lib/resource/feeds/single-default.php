<?php

	echo '<?xml version="1.0" encoding="UTF-8"?>';

	$feedURL=str_replace('feed/', '', BASE_URL);

	if($categoryData=mysqli_query($connection, "SELECT category_id, category_title, link_url as category_url FROM {$table_prefix}category, {$table_prefix}link WHERE link_url='{$feedURL}' AND link_relation_id=category_id AND link_status=200 AND link_type='category'")) {

		if(mysqli_num_rows($categoryData)!=0) {

			extract(mysqli_fetch_assoc($categoryData));

			if($findPost=mysqli_query($connection, "SELECT link_id, link_url, link_published, link_modified, post_title, post_description, post_image, profile_image, profile_display_name, meta_description FROM {$table_prefix}post INNER JOIN {$table_prefix}link ON link_relation_id=post_id INNER JOIN {$table_prefix}profile ON profile_id=post_owner_id INNER JOIN {$table_prefix}meta ON meta_relation_id=post_id WHERE post_category={$category_id} AND link_type='post' ORDER BY link_modified DESC LIMIT 0, 10")) {

				if(mysqli_num_rows(($findPost))!=0) { ?>
					
					<rss version="2.0"
					xmlns:content="http://purl.org/rss/1.0/modules/content/"
					xmlns:wfw="http://wellformedweb.org/CommentAPI/"
					xmlns:dc="http://purl.org/dc/elements/1.1/"
					xmlns:atom="http://www.w3.org/2005/Atom"
					xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
					xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
					xmlns:media="http://search.yahoo.com/mrss/"
					>

					<image>
						<url><?php echo USER_IMAGES.'logo.png' ?></url>
						<title><?php echo SITE_NAME; ?></title>
						<link><?php echo SITE_URL ?></link>
						<width>32</width>
						<height>32</height>
					</image> 

					<?php
					$i=1;
					while($postData=mysqli_fetch_assoc($findPost)) {
						
						if($i==1) { ?>

							<channel>
								<title><?php echo $category_title ?></title>
								<atom:link href="<?php echo SITE_URL.'/feed'.$category_url ?>" rel="self" type="application/rss+xml" />
								<link><?php echo SITE_URL ?></link>
								<description><?php echo SITE_DESC ?></description>
								<lastBuildDate><?php echo date('D, M d, Y H:i A', strtotime('+5 hour +30 minutes', strtotime($postData['link_published']))) ?></lastBuildDate>
								<language>hi</language>
								<sy:updatePeriod>hourly</sy:updatePeriod>
								<sy:updateFrequency>1</sy:updateFrequency>

						<?php
						} ?>

						<item>
							<Articleid><?php echo $postData['link_id']; ?></Articleid>
							<title><![CDATA[<?php echo $postData['post_title']; ?>]]></title>
							<link><![CDATA[<?php echo SITE_URL.$postData['link_url']; ?>]]></link>
							<authorname><?php echo $postData['profile_display_name'] ?></authorname>
							<thumbimage><![CDATA[<?php echo SITE_URL.$postData['post_image'] ?>]]></thumbimage>
							<fullimage><![CDATA[<?php echo SITE_URL.$postData['post_image'] ?>]]></fullimage>
							<pubDate><?php echo date('r', strtotime('+5 hour +30 minutes', strtotime($postData['link_published']))) ?></pubDate>
							<modifiedDate><?php echo date('r', strtotime('+5 hour +30 minutes',strtotime($postData['link_modified']))) ?></modifiedDate>
							<description><![CDATA[<?php echo str_replace('<img src="', '<img src="'.SITE_URL.'', html_entity_decode($postData['post_description'])); ?>]]></description>
							<excerpt><![CDATA[<?php echo $postData['meta_description']; ?>]]></excerpt>
							<tags><?php echo $postData['tagName']; ?></tags>
						</item>

					<?php
						$i++;
					}

					echo '</channel></rss>';
				

				}

			}

		}

	} else {
		echo mysqli_error($connection);
	}

?>