<?php

	$tag_name=str_replace('-', ' ', $urlStructure[2]);
	
    if($tagList=mysqli_query($connection, "SELECT link_url, link_published, link_modified, post_id, post_image, post_title, post_description, post_category, category_title, post_owner_id, post_type, meta_title, meta_description, meta_canonical, meta_keyword, meta_robots, meta_geo_position, meta_geo_place_name, meta_geo_region, fb_img, fb_title, fb_desc, tw_img, tw_title, tw_desc,  profile_display_name, profile_image FROM {$table_prefix}link INNER JOIN {$table_prefix}post ON link_relation_id=post_id INNER JOIN {$table_prefix}meta ON meta_relation_id=post_category INNER JOIN {$table_prefix}category ON category_id=post_category INNER JOIN {$table_prefix}profile ON profile_id=post_owner_id INNER JOIN {$table_prefix}user ON profile_owner_id=user_id INNER JOIN {$table_prefix}tag ON tag_relation_id=post_id WHERE link_type='post' AND tag_type='post' AND tag_name='{$tag_name}' ORDER BY link_modified DESC LIMIT 0, 10")) {

        if(mysqli_num_rows($tagList)!=0) { ?>

            <rss version="2.0"
				xmlns:content="http://purl.org/rss/1.0/modules/content/"
				xmlns:wfw="http://wellformedweb.org/CommentAPI/"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:atom="http://www.w3.org/2005/Atom"
				xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
				xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
				xmlns:media="http://search.yahoo.com/mrss/"
				>

				<image>
					<url><?php echo USER_IMAGES.'logo.png' ?></url>
					<title><?php echo SITE_NAME; ?></title>
					<link><?php echo SITE_URL ?></link>
					<width>32</width>
					<height>32</height>
				</image>

            <?php
            
            $i=1;

            while($tagPostData=mysqli_fetch_assoc($tagList)) {

                extract($tagPostData);

                if($i==1) { ?>

                    <channel>
						<title><?php echo $tag_name ?></title>
						<atom:link href="<?php echo SITE_URL.'/feed/'.$urlStructure[2] ?>/" rel="self" type="application/rss+xml" />
						<link><?php echo SITE_URL ?></link>
						<description><?php echo SITE_DESC ?></description>
						<lastBuildDate><?php echo date('r', strtotime('+5 hour +30 minutes', strtotime($link_published))) ?></lastBuildDate>
						<language>hi_IN</language>
						<sy:updatePeriod>hourly</sy:updatePeriod>
						<sy:updateFrequency>1</sy:updateFrequency>

                <?php
                }

                ?>

                <item>
					<Articleid><?php echo $post_id; ?></Articleid>
					<title><?php echo $post_title; ?></title>
					<link><?php echo SITE_URL.$link_url; ?></link>
					<thumbimage><![CDATA[<?php echo SITE_URL.$post_image; ?>]]></thumbimage>
					<fullimage><![CDATA[<?php echo SITE_URL.$post_image; ?>]]></fullimage>
					<pubDate><?php echo date('r', strtotime('+5 hour +30 minutes', strtotime($link_published))) ?></pubDate>
					<modifiedDate><?php echo date('r', strtotime('+5 hour +30 minutes',strtotime($link_modified))) ?></modifiedDate>
					<description><![CDATA[<?php echo str_replace('src="', 'src="'.SITE_URL.'', html_entity_decode($post_description)); ?>]]></description>
					<authorname><?php echo $profile_display_name ?></authorname>
					<excerpt><?php echo $meta_description; ?></excerpt>
				</item>

                <?php

                $i++;
                
            }

            echo '</channel></rss>';

        } else {

        }

	}