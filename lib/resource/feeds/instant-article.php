<?php

    echo "<rss version='2.0'
            xmlns:content='http://purl.org/rss/1.0/modules/content/'>
            <channel>
                <title>".SITE_NAME."</title>
                <link>".SITE_URL."</link>
                <description>".SITE_DESC."</description>
                <language>hi_IN</language>
                <lastBuildDate>2014-12-11T04:44:16Z</lastBuildDate>";
                
                if($postData=mysqli_query($connection, "SELECT post_id, post_image, post_title, post_description, post_category, category_title, post_owner_id, post_type, meta_title, meta_description, meta_canonical, meta_keyword, meta_robots, meta_geo_position, meta_geo_place_name, meta_geo_region, fb_img, fb_title, fb_desc, tw_img, tw_title, tw_desc, link_status, link_type, link_published, link_modified, link_url, profile_display_name FROM {$table_prefix}link INNER JOIN {$table_prefix}site ON site_id=link_site_id INNER JOIN {$table_prefix}post ON link_relation_id=post_id INNER JOIN {$table_prefix}meta ON meta_relation_id=post_id INNER JOIN {$table_prefix}category ON category_id=post_id INNER JOIN {$table_prefix}profile ON profile_id=post_owner_id INNER JOIN {$table_prefix}user ON profile_owner_id=user_id WHERE link_type='post' ORDER BY link_modified DESC LIMIT 0, 10")) {
                    /* Article feed section */
                    
                    while($postListData=mysqli_fetch_assoc($postData)) { extract($postListData); ?>


                        <item>
                            <title><?php echo $post_title ?></title>
                            <link><?php echo SITE_URL.$link_url ?></link>
                            <guid><?php echo $post_id ?></guid>
                            <pubDate><?php echo date('Y-m-dTH:i:sZ', strtotime($link_published)); ?></pubDate>
                            <author><?php echo $profile_display_name ?></author>
                            <description><?php $meta_description ?></description>
                            <content:encoded>
                                <![CDATA[
                                    <!doctype html>
                                    <html lang="en" prefix="op: http://media.facebook.com/op#">
                                    <head>
                                        <meta charset="utf-8">
                                        <link rel="canonical" href="<?php echo SITE_URL.$link_url ?>">
                                        <meta property="op:markup_version" content="v1.0">
                                    </head>
                                    <body>
                                        <article>

                                            <header>
                                                <figure><img src="https://punjabkesari.com/wp-content/uploads/2019/05/1-42.jpg"></figure><h1>टिक-टॉक के सेलिब्रिटी Mohit Mor की हत्या में नया मोड़,कहीं असली वजह के पीछे ये शख्स तो नहीं</h1>
                                                
                                                <time class="op-published" datetime="<?php echo date('Y-m-dTH:i:s+00:00', strtotime($link_published)); ?>"><?php echo date('M dS, H:ia', strtotime($link_published)); ?></time><time class="op-modified" datetime="2019-05-22T19:15:39+00:00">May 22nd, 7:15pm</time><address><a>Ayesha Chauhan</a></address>
                                                
                                                <h3 class="op-kicker">दिलचस्प खबरें</h3>

                                                <!-- Facebook Direct Ads -->
                                                <figure class="op-ad"><iframe src="https://www.facebook.com/adnw_request?placement=1556823401016138_2545049242193544&amp;adtype=banner300x250" width="300" height="250"></iframe></figure>

                                            </header>

                                            <?php echo str_replace('">', '"></figure>', str_replace('<img src="', '<figure><img src="https://punjabkesari.com' , $post_description)); ?>

                                            <!— Google Analytics Code -->
                                            <figure class="op-tracker">
                                                <iframe>
                                                    <script>

                                                        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                                        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                                                        ga('create', 'UA-10313152-1', 'auto');
                                                        ga('send', 'pageview');


                                                    </script>
                                                </iframe>
                                            </figure>

                                            <footer>
                                                <small>Copyright © <?php echo date('Y').' '.SITE_NAME ?>. All rights reserved. </small>
                                            </footer>
                                        </article>
                                    </body>
                                    </html>
                                ]]>
                            </content:encoded>
                        </item>

                    <?php
                    }

                    echo "</channel></rss>";

                }